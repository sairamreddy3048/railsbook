class AddPictureToMicroPosts < ActiveRecord::Migration[5.1]
  def change
    add_column :micro_posts, :Picture, :string
  end
end

class AddIndexToUsersUser < ActiveRecord::Migration[5.1]
  def change
    add_index :users, :user, unique: true
  end
end

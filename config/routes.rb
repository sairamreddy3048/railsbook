Rails.application.routes.draw do

  resources :topics
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :users
    end
  end

  root "pages#homepage"
  get '/home',to: 'pages#homepage'
  get '/help', to: 'pages#helppage'
  get '/about', to: 'pages#aboutpage'
  get '/contact',to: 'pages#contactpage'
  get '/signup',to: 'users#new'
  post '/signup',  to: 'users#create'
  patch '/makeadmin', to: 'users#make_admin', as: 'makeadmin'
  get '/login', to:  'sessions#new'
  post '/login', to:  'sessions#create'
  delete  '/logout', to:  'sessions#destroy'
  resources :account_activations, only: [:edit]
  resources :users  do
    member do
      get :following # like users/1/following
      get :followers
    end
  end
  resources :password_resets, only: [:new,:create,:edit,:update]
  resources :micro_posts, only: [:create,:destroy]
  get '/micro_posts', to: 'micro_posts#index', :defaults => { :format => 'json' }
  get '/show_users_json', to: 'users#show_users_json', :defaults => { :format => 'json' }
  get '/show_user_json/:id', to: 'users#show_user_json', :defaults => { :format => 'json' }
  resources :relationships, only: [:create,:destroy]
  resources :ratings, only: [:create]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end

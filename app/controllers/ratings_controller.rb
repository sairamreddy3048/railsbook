class RatingsController < ApplicationController
  before_action :logged_in_user
  def create
    @id = params[:micropost_id]
    @micropost = MicroPost.find(@id)
     if @micropost.valid?
      @rating = params[:star]
      @micropost.ratings.create(rating: @rating)
      p @micropost.avg_ratings
       respond_to do |format|
         format.html {redirect_to home_url}
         format.js
       end
     else
      flash[:danger] = "Post is not available"
       redirect_to root_path
    end
  end
end
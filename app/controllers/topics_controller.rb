class TopicsController < ApplicationController
  before_action :set_topic, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  respond_to :html

  def index
    @topics = Topic.all
    # authorize! :index, @topics
  end

  def show
    @topic = Topic.find(params[:id])
    @microposts = @topic.micro_posts.paginate(page: params[:page],per_page: 10)
    authorize! :show, @topic
  end

  def new
    @topic = Topic.new
    # respond_with(@topic)
    # authorize! :new, @topic
  end

  def edit
  end

  def create
    @topic = Topic.new(topic_params)
    @topic.save
    respond_with(@topic)
    authorize! :create, @topic

  end

  def update
    @topic.update(topic_params)
    respond_with(@topic)
    authorize! :update, @topic

  end

  def destroy
    @topic.destroy
    respond_with(@topic)
    authorize! :destroy, @topic

  end

  private
    def set_topic
      @topic = Topic.find(params[:id])
    end

    def topic_params
      params.require(:topic).permit(:name)
    end
end

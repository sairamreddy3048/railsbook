module Api
  module V1
    class UsersController < ApplicationController
      respond_to :json

      def index
        # render json: {user: User.all}
        respond_with User.all
      end

      def show
        respond_with User.find(params[:id])
      end


      def destroy
        respond_with User.destroy(params[:id])

      end
    end
  end
end
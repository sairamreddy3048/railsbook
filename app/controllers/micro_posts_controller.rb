class MicroPostsController < ApplicationController
  before_action :logged_in_user , only: [:create,:destroy]
  before_action :correct_user , only: :destroy


  def index
    @microposts = MicroPost.all
  end

  def create
    @micropost = current_user.micro_posts.build(micropost_params)
    @microposts = current_user.micro_posts.paginate(page: params[:page],per_page: 10)
    if @micropost.save
      @saved = true
      respond_to do |format|
        format.html {redirect_to root_path}
        format.js
      end
    else
      @saved = false
      respond_to do |format|
        format.html {redirect_to root_path}
        format.js
      end
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "Tweet has been deleted successfully"
    redirect_to request.referrer || root_url   # for redirecting to the old page from where the request has came
  end

  private

  def micropost_params
    params.require(:micro_post).permit(:content,:Picture,:topic_id)
  end

  def correct_user
    @micropost = current_user.micro_posts.find_by(id:params[:id])
    redirect_to root_path if @micropost.nil?
  end
end

class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:edit,:update]
  before_action :valid_user, only: [:edit,:update]
  before_action :check_expiration, only: [:edit,:update]
  def new
  end

  def create
    @user = User.find_by(user: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_mail
      flash[:info] = "Reset password link has been sent to your email"
      redirect_back_or(root_path)
    else
      flash.now[:danger] = "Please enter valid email address"
      render 'new'
    end
  end

  def edit

  end


  def update
    if params[:user][:password].empty?
      @user.errors.add(:password,"can't be empty")
      render 'edit'
    elsif @user.update_attributes(user_params)
      log_in @user
      @user.update_attribute(:reset_digest,nil)
      flash[:success] = "Password Reset is successful"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

  def user_params
    params.require(:user).permit(:password,:password_confirmation)
  end

  def get_user
    @user = User.find_by(user: params[:email])
  end

  def valid_user
    unless @user && @user.activated && @user.authenticated?(:reset,params[:id])
      redirect_to root_path
    end
  end

  def check_expiration
    if @user.reset_token_expired?
      flash[:danger] = "Password Reset has expired .. Please try again"
      redirect_to new_password_reset_path
    end
  end


end
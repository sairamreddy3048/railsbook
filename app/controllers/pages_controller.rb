class PagesController < ApplicationController
  def homepage
    if logged_in?
      @micropost = current_user.micro_posts.build
      @microposts= current_user.feed.paginate(page: params[:page],per_page: 10)
    end
    @device = request.user_agent
  end

  def helppage
  end

  def aboutpage
  end
  
  def contactpage
  end

end

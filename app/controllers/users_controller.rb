class UsersController < ApplicationController
  before_action :logged_in_user , only: [:index,:edit,:update,:destroy,:show,:following,:followers]
  before_action :correct_user , only: [:edit, :update]
  before_action :is_admin?, only: [:destroy]
  before_action :check_old_password, only: [:update]
  # skip_before_action :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }


  def index
    if params[:search]
      @users = User.paginate(page: params[:page], :per_page => 10).search(params[:search]).order("created_at DESC")
      if @users.blank?
        flash[:info] = "No users with name containing #{params[:search]}"
        @users = User.paginate(page: params[:page], :per_page => 10).where(activated: true)
      end
    else
      @users = User.paginate(page: params[:page], :per_page => 10).where(activated: true)
    end
  end
# for Json Api
  def show_users_json
    @users = User.all
    # respond_to do |format|
    #   format.json {}
    # end
  end
  def show_user_json
    @user = User.find(params[:id])
    @microposts = @user.micro_posts.all;
    # respond_to do |format|
    #   format.json {}
    # end
  end

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
    @microposts = @user.micro_posts.paginate(page: params[:page], :per_page => 10)
    if !@user.activated
      flash[:warning] = "User account is not yet active"
      redirect_to root_url
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_mail
      flash[:info] = "Please check your email to activate your account."
      respond_to do |format|
      format.html {redirect_to root_url}
      format.json {redirect_to login_url }
      end
    else
      render 'new'
    end

  end

  def edit
    @user = User.find(params[:id])
  end


  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Your Details have been Updated Successfully " + @user.name + " !"
      respond_to do |format|
        format.html {redirect_to root_url}
        format.json {redirect_to root_path }
      end
    else
      render 'edit'
    end
  end

  def destroy
    if is_admin? && !User.find(params[:id]).superadmin?
      User.find(params[:id]).destroy
      flash[:success] = "User has been Deleted Successfully"
      respond_to do |format|
        format.html {redirect_to users_path}
        format.json {render :json => {:name => "David"}.to_json}
      end
    else
      flash[:danger] = "You will be severely punished"
      redirect_to root_path
    end
  end



  #For making the user as ADMIN
    def make_admin
      if is_superadmin?
        @user = User.find(params[:id])
        if params[:status] == "undo"
          @user.update_attributes(admin:false)
          flash[:info] = "Removed admin status for #{@user.name}"
          redirect_to users_path
        elsif @user.update_attributes(admin:true)
          flash[:success] = "#{@user.name} has been made as admin"
          redirect_to users_path
        end
      end
    end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page],per_page: 10)
    @all_users = @user.following.all
    respond_to do |format|
      format.html {render 'show_follow'}
      format.pdf do
        pdf = FollowersPdf.new(@all_users,"Followings")
        send_data pdf.render, filename: "followings_#{@user.name}.pdf", type: 'application/pdf'
      end
      format.json {}
    end
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page],per_page: 10)
    @all_users = @user.followers.all  # for pdf
    respond_to do |format|
      format.html {render 'show_follow'}
      format.pdf do
        pdf = FollowersPdf.new(@all_users,"Followers")
        send_data pdf.render, filename: "followers_#{@user.name}.pdf", type: 'application/pdf'
      end
      format.json {}
    end
  end

  private

    def user_params
      params.require(:user).permit(:name,:user,:password,:password_confirmation)
    end

  #Before Filters

    def correct_user
      @user = User.find(params[:id])
       unless current_user?(@user)
         redirect_to root_path
         flash[:warning] = "You dont have access rights to update this record"
       end
    end

    def is_admin?
      current_user.admin?
    end

    def is_superadmin?
      current_user.superadmin?
    end

   def check_old_password
    if  @user.authenticated?(:password,params[:user][:password])
      flash[:warning] = "Please use new password"
      render 'edit'
    end
    end

end

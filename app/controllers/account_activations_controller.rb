class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(user: params[:email])
    if user && !user.activated && user.authenticated?(:activation,params[:id])
      flash[:success] = "Account activation is successful.. Welcome!!"
      log_in(user)
      user.activate
      redirect_to user
    else
      flash[:danger] = "Please Try Again. Unsuccessful"
      redirect_to root_url
    end
  end

end

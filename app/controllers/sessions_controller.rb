class SessionsController < ApplicationController
  before_action :logged_in?, only: [:new]

  def new
  end

  def create
    @user = User.find_by(user: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      if @user.activated?
        log_in @user
        redirect_back_or(@user)
      else
        flash[:warning] = "Account Not Activated Please check  your mail"
        redirect_to root_url
      end
    else
      flash.now[:danger] = "Invalid Email / Password "
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end

  private
  def logged_in?
    unless current_user.nil?
      flash[:info]="You are already logged in!"
      redirect_back_or(users_path)
    end
  end
end

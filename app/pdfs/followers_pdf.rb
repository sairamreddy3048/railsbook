
class FollowersPdf < Prawn::Document
  def initialize(followers,relation)
    super()
    @followers = followers
    @relationship = relation
    header
    table_content
  end

  def header
    #This inserts an image in the pdf file and sets the size of the image
    image "#{Rails.root}/app/assets/images/header.png", width: 530, height: 150
    text "\n#{@relationship}", :size => 25, :align => :center
  end


  def table_content
    # This makes a call to product_rows and gets back an array of data that will populate the columns and rows of a table
    # I then included some styling to include a header and make its text bold. I made the row background colors alternate between grey and white
    # Then I set the table column widths
    table product_rows do
      row(0).font_style = :bold
      self.header = true
      self.row_colors = ['DDDDDD', 'FFFFFF']
      self.column_widths = [40, 300, 200]
    end
  end

  def product_rows
    [['#', 'Name', 'Tweets made upto now']] +
        @followers.map do |follower|
          [follower.id, follower.name, follower.micro_posts.count]
        end
  end
end
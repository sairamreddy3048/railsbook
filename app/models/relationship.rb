class Relationship < ApplicationRecord
  belongs_to :follower, class_name: "User"
  belongs_to :followed, class_name: "User"
  default_scope -> {order(created_at: :desc)}  # To show most recent posts first
  validates :follower_id, presence: true
  validates :followed_id, presence: true


end

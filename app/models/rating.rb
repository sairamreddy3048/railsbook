class Rating < ApplicationRecord
  belongs_to :MicroPost, optional: true
  validates :rating, presence:true ,:inclusion => 1..5


end

class MicroPost < ApplicationRecord
  belongs_to :user
  belongs_to :topic
  has_many :ratings
  default_scope -> {order(created_at: :desc)}  # To show most recent posts first
  mount_uploader :Picture, PictureUploader
  validates :user_id, presence: true
  validates :content ,presence: true, length: {maximum: 140}
  validate :picture_size

  def avg_ratings
    @sum = 0
    @total = 0.0
    all_ratings = Rating.where(micro_post_id: id).group(:rating).count
    all_ratings.each do |key,value|
      @sum += key*value
      @total += value
    end
    if @total > 0
      return (@sum/@total).round(1)
    else
      return 0;
    end
  end

    private

  def picture_size
    if self.Picture.size > 5.megabytes
      errors.add(:Picture,"Should be less than 5 mb")
    end
  end


end

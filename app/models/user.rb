class User < ApplicationRecord
  has_many :micro_posts, dependent: :destroy
  has_many :active_relationships, class_name: "Relationship", foreign_key: "follower_id", dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :passive_relationships, class_name: "Relationship",
           foreign_key: "followed_id", dependent: :destroy
  has_many :followers, through: :passive_relationships, source: :follower
  attr_accessor :activation_token, :reset_token
  before_save :email_downcase
  before_create :create_activation_digest
  validates :name, presence: true, length: {maximum: 50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :user, presence: true, length: {maximum: 255}, format: {with: VALID_EMAIL_REGEX}, uniqueness: {case_sensitive: false}
  validates :password, :format => {:with => /\A(?=.*\d)(?=.*([a-z]|[A-Z]))([\x20-\x7E]){8,40}\z/, message: "must be at least 6 characters and include one number and one letter."},
            :presence => true,
            :confirmation => true,
            :length => {:within => 5..40},
            :on => [:create, :update], allow_nil: true
  has_secure_password

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest: User.digest(reset_token), reset_sent_at: Time.zone.now)
  end

  def reset_token_expired?
    reset_sent_at < 10.minutes.ago
  end

  def send_password_reset_mail
    UserMailer.password_reset(self).deliver_now
  end


  def activate
    update_attributes(activated: true, activated_at: Time.zone.now)
    # update_attribute(:activated,    true)
    # update_attribute(:activated_at, Time.zone.now)
  end

  def send_mail
    UserMailer.account_activation(self).deliver_now
  end

  #for showing all microposts
  def feed
    following_ids = "SELECT followed_id FROM relationships WHERE follower_id = :user_id"
    MicroPost.where("user_id IN (#{following_ids}) OR user_id = :user_id", user_id: id) # for avoiding sql injection  , normally following_ids contain the list of all users followed by default but we are using query to improve performance
  end

  def follow(other_user)
    following << other_user
  end

  def unfollow(other_user)
    following.delete(other_user)
  end

  def following?(other_user)
    following.include?(other_user)
  end

  def self.search(search)
    where("name LIKE ?", "%#{search}%")
  end

  class << self

    def User.digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
          BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end

    def User.new_token
      SecureRandom.urlsafe_base64
    end

  end

  private

  def email_downcase
    self.user = user.downcase
  end

  def create_activation_digest
    self.activation_token= User.new_token
    self.activation_digest= User.digest(activation_token)
  end

end

class ApplicationMailer < ActionMailer::Base
  default from: 'sairamreddy3048@gmail.com'
  layout 'mailer'
end

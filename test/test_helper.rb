ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  include ApplicationHelper
  fixtures :all

  def is_logged_in?
    !session[:user_id].nil?
  end

  # def log_in_as(user)
  #
  # end

  # Add more helper methods to be used by all tests here...
end

class ActionDispatch::IntegrationTest

  # Log in as a particular user.
  def log_in_as(user, password: 'Kings@123')
    post login_path, params: { session: { email: user.user,
                                          password: password } }
    session[:user_id] = user.id
  end
end

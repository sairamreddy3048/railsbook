require 'test_helper'

class FollowingTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:sairam)
    @other = users(:murali)
    log_in_as(@user)
    puts @user.id
  end

  test "Following page" do
    get following_user_path(@user)
    assert_not @user.following.empty?
    assert_match @user.following.count.to_s, response.body
    @user.following.each do |user|
      assert_select "a[href=?]",user_path(user)
    end
  end

  test "Follower page" do
    get followers_user_path(@user)
    assert_not @user.followers.empty?
    assert_match @user.followers.count.to_s, response.body
    @user.followers.each do |user|
      assert_select "a[href=?]",user_path(user)
    end
  end

  test "Should follow user with ajax" do
      assert_difference '@user.following.count', 1 do
      post relationships_path, xhr: true, params: { followed_id: @other.id }
      end
  end

  test "Should delete user with ajax" do
    @user.follow(@other)
    relationship = @user.active_relationships.find_by(followed_id: @other.id)
      assert_difference '@user.following.count',-1 do
      delete relationship_path(relationship),xhr: true
    end
  end
end

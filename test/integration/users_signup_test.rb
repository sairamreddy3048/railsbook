require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "SHould not Insert the invalid User" do
    get signup_path
    assert_no_difference 'User.count' do
      post signup_path,params: {user: {name:"sai",user:"dsf@dsh.com",password:"12",password_confirmation: "12"}}
    end
    assert_template 'users/new'
  end

  test "Should save the valid record" do
    get signup_path
    record_count = User.count
    User.create(name:"murali",user:"muralireddy@gmail.com",password:"Kings@123",password_confirmation: "Kings@123")
    assert_equal record_count+1,User.count
  end

  test "Valid Signup using Post" do
    get signup_path
    assert_difference 'User.count',1 do
      post users_path, params: { user: {name: "sastra",
                                        user: "sasra@edu.com",
                                        password:"Kings@123",
                                        password_confirmation:"Kings@123"} }
    end
    follow_redirect!
    assert_template "pages/homepage"
    assert_not flash.empty?
    assert_not is_logged_in?
  end

  test "Valid signup information along with account activation" do
    get signup_path
    assert_difference 'User.count', 1 do
    post users_path,params:{user: {name:"Sairam Reddy",user:"Ksairamreddy48@gmail.com",password:"Kings@123",password_confirmation:"Kings@123"}}
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    get edit_account_activation_path("invalid token", email: user.user)
    assert_not is_logged_in?
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    get edit_account_activation_path(user.activation_token, email: user.user)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end
end

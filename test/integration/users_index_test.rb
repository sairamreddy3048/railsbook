require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:sairam)
    @second_user = users(:murali)
  end

  test "Index including Pagination" do
    log_in_as(@user)
    get users_path
    assert_template "users/index"
    assert_select "div.pagination"
    User.paginate(page: 1, per_page: 10).each do |user|
      assert_select "a[href=?]",user_path(user)
    end
  end

  test "Index as admin including pagination and delete links" do
    log_in_as(@user)
    get users_path
    assert_template "users/index"
    assert_select "div.pagination"
    first_page = User.paginate(page:1, per_page: 10)
    first_page.each do |user|
      assert_select "a[href=?]",user_path(user), text: user.name
      unless user == @user
        assert_select "a[href=?]",user_path(user), text: "Delete"
      end
    end
    assert_difference 'User.count', -1 do
      delete user_path(@second_user)
    end
  end

  test "Index as non admin users" do
    log_in_as(@second_user)
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    get users_path
    assert_select "a",text:"delete",count: 0
  end

end

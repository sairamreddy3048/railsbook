require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:sairam)
    puts @user.micro_posts.count
  end

  test "Profile Display" do
   log_in_as(@user)
    get user_path(@user)
    assert_template "users/show"
    assert_select "title","sairamreddy Profile"
    assert_select ".panel-primary",count: 10
    @user.micro_posts.paginate(page: 1, per_page: 10).each do |post|
      assert_match post.content,response.body
    end
  end

  test "Should increase the following" do
    log_in_as(@user)
    @user.follow(users(:murali))
    get root_path
    assert_template "pages/homepage"
    assert_select "strong","1"
  end
end

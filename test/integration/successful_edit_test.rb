require 'test_helper'

class SuccessfulEditTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:sairam)
  end

  test "Should not edit/update invalid records" do
    log_in_as @user
    get edit_user_path(@user)
    assert_template "users/edit"
    patch user_path,params: {user: {name:"",user: "ram",password: "123",password_confirmation: "123"}}
    assert_template "users/edit"
    assert_select "div.alert", "The form contains 4 errors"
  end

  test "Should update valid record" do
    log_in_as @user
    get edit_user_path(@user)
    assert_template "users/edit"
    patch user_path,params: {user: {name:"SAIRAM REDDY",user:@user.user,password:"Kings@1234",password_confirmation:"Kings@1234"}}
    assert_not flash.empty?
    assert_redirected_to root_path
    follow_redirect!
    assert_template "pages/homepage"
    @user.reload
    assert_equal @user.name,"SAIRAM REDDY"
    assert_equal @user.user,"ram48484@gmail.com"
  end

  test "Successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as @user
    assert_redirected_to edit_user_url(@user)
    name = "K SAIRAM REDDY"
    patch user_path(@user),params: {user: {name: name,user:@user.user,password:"Kings@1234",password_confirmation:"Kings@1234"}}
    assert_not flash.empty?
    assert_redirected_to root_path
    @user.reload
    assert_equal name,@user.name
  end
end

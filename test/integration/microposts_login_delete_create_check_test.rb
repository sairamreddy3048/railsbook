require 'test_helper'

class MicropostsLoginDeleteCreateCheckTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:sairam)
  end

  test "Micrpost Interface" do
    get login_path
    assert_template "sessions/new"
    log_in_as(@user)
    get root_path
    assert_select "div.pagination"
    #Invalid Submission
    assert_no_difference 'MicroPost.count' do
      post micro_posts_path,params:{micro_post:{content:""}}
    end
    assert_select "div#error-explaination"
    #valid submission
    content = "Hey how are you?"
    assert_difference 'MicroPost.count',1 do
      post micro_posts_path,params:{micro_post:{content: content}}
    end
    assert_redirected_to @user
    follow_redirect!
    assert_match content,response.body
    #Delete Post
    assert_select 'a' , text: 'Delete Post'
    first_micropost = @user.micro_posts.paginate(page:1,per_page:10).first
    assert_difference 'MicroPost.count',-1 do
      delete micro_post_path(first_micropost)
    end
    get user_path(users(:murali))
    assert_select 'a', text: 'Delete Post',count:0
  end

  test "checking the image upload" do
    get root_path
    log_in_as(@user)
    assert_difference 'MicroPost.count',1 do
    post micro_posts_path,params:{micro_post:{content:"Hello ",Picture:"https://upload.wikimedia.org/wikipedia/en/4/42/Sastra_logo.png"}}
    end
    assert_redirected_to @user
    follow_redirect!
    assert'div.image_align'
  end
end

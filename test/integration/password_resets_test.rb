require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:sairam)
    p @user.class
  end

  test "Password resets test" do
    get new_password_reset_path
    assert_template "password_resets/new"
    #Invalid Email
    post password_resets_path, params: { password_reset: {email: "123"} }
    assert_not flash.empty?
    assert_template "password_resets/new"
    #valid email
    post password_resets_path, params: {password_reset: {email:@user.user}}
    assert_not_equal @user.reset_digest,@user.reload.reset_digest
    assert_not flash.empty?
    assert_redirected_to root_path
    # password reset form
    @user = assigns(:user)
    get edit_password_reset_path(@user.reset_token,"")
    assert_redirected_to root_path
    get edit_password_reset_path('wrong token', email: @user.user)
    assert_redirected_to root_url
    get edit_password_reset_path(@user.reset_token, email: @user.user)
    assert_template 'password_resets/edit'
    assert_select "input[name=email][type=hidden][value=?]", @user.user
    # Invalid password & confirmation
    patch password_reset_path(@user.reset_token),
          params: { email: @user.user,
                    user: { password:              "foobaz",
                            password_confirmation: "barquux" } }
    assert_select 'div#error-explaination'
    # not valid password
    patch password_reset_path(@user.reset_token),
          params: { email: @user.user,
                    user: { password:              "foobaz",
                            password_confirmation: "foobaz" } }
    assert_select 'div#error-explaination'
    # valid password
    patch password_reset_path(@user.reset_token),
          params: { email: @user.user,
                    user: { password:              "Kings@123",
                            password_confirmation: "Kings@123" } }
    assert is_logged_in?
    assert_not flash.empty?
    assert_redirected_to user_path(@user)
  end

  test "Expiration token" do
    get new_password_reset_path
    post password_resets_path, params: {password_reset: {email:@user.user}}

    @user = assigns(:user)
    @user.update_attribute(:reset_sent_at,3.hours.ago)
    patch password_reset_path(@user.reset_token), params: { email:@user.user, user:{password: "Kings@123",password_confirmation: "Kings@123"}}
    assert_response :redirect
    follow_redirect!
    assert_not flash.empty?
    assert_match "expired",response.body
  end
end

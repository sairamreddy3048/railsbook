require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:sairam)
  end

  test "Login with Invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path,params: {session: {email:"",password:"123"}}
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty? , "Flash is not empty"
  end

  test "Login with valid information" do
    get login_path
    assert_template "sessions/new"
    post login_path,params: {session: {email:@user.user,password:'Kings@123'}}
    assert_redirected_to @user
    follow_redirect!
    assert_template "users/show"
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
  end

  test "Login and Logout" do
    get login_path
    assert_template "sessions/new"
    post login_path,params: { session: {email:@user.user,password:'Kings@123'}}
    assert_redirected_to @user
    follow_redirect!
    assert_template "users/show"
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", user_path(@user), count: 11
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", user_path(@user), count: 0

  end

  test "Should not store the requested url for the second time" do
    get edit_user_path(@user)
    assert_redirected_to login_path
    log_in_as @user
    assert_redirected_to edit_user_path(@user)
    delete logout_path
    assert_redirected_to root_url
    get login_path
    log_in_as @user
    assert_redirected_to @user
  end
end


require 'test_helper'

class MicropostsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @micropost = micro_posts(:orange)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'MicroPost.count' do
      post micro_posts_path, params: { micropost: { content: "Lorem ipsum" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'MicroPost.count' do
      delete micro_post_path(@micropost)
    end
    assert_redirected_to login_url
  end


test "should redirect invalid destroy request" do
  log_in_as users(:sairam)
  micropost = micro_posts(:cat_video)
    assert_no_difference 'MicroPost.count' do
    delete micro_post_path(micropost)
  end
  assert_redirected_to root_url
  end
end
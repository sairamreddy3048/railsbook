require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @constant = "page"
  end

  test "should get root page" do
    get root_url
    assert_response :success
  end

  test "should get homepage" do
    get root_path
    assert_response :success
    assert_select "title","Ruby on Rails"
  end

  test "should get helppage" do
    get help_path
    assert_response :success
    assert_select "title","Help#{@constant}"
  end

  test "should get aboutpage" do
    get about_path
    assert_response :success
    assert_select "title","About#{@constant}"
  end

  test "should get contact page" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact#{@constant}"
  end

end

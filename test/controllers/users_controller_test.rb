require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:sairam)
    @second_user = users(:murali)
  end

  test "should get new" do
    get signup_path
    assert_response :success
    assert_select "title",full_title("Signup page")
  end

  test "should not edit before logging in" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "Should not update before logged in" do
    patch user_path(@user),params: {user: {name: @user.name,user: "hello@mail.com",password: "",password_confirmation: ""}}
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect to home page" do
    log_in_as(@user)
    get edit_user_path(@second_user)
    assert_not flash.empty?
    assert_redirected_to root_path
  end

  test "Should not update the others Profile" do
    log_in_as(@user)
    patch user_path(@second_user),params:{user: {name:"mullu",user:"radhsd",password:"",password_confirmation:""}}
    assert_not flash.empty?
    assert_redirected_to root_path
  end

  test "Should Show Index Page" do
    get users_path
    assert_redirected_to login_url
  end

  test "Should not accept the admin attribute through Web" do
    log_in_as(@second_user)
    patch user_path(@second_user),params:{user:{name:@second_user.name,user:@second_user.user,password:"",password_confirmation:"",admin:true}}
    assert_not flash.empty?
    assert_not @second_user.reload.admin?
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end

  test "Shoud redirect destroy when logged in as non admin" do
    log_in_as @second_user
    assert_no_difference 'User.count' do
      delete user_path(@second_user)
    end
    assert_redirected_to root_path
  end

  test "should redirect following when not logged in" do
    get following_user_path(@user)
    assert_redirected_to login_url
  end

  test "should redirect followers when not logged in" do
    get followers_user_path(@user)
    assert_redirected_to login_url
  end

end

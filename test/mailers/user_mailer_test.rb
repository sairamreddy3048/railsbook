require 'test_helper'

class UserMailerTest < ActionMailer::TestCase

  def setup
    @user = users(:sairam)
  end

  test "account_activation" do
    @user.activation_token = User.new_token
    mail = UserMailer.account_activation(@user)
    assert_equal "Account Activation", mail.subject
    assert_equal ["ram48484@gmail.com"], mail.to
    assert_equal ["sairamreddy3048@gmail.com"], mail.from
    assert_match @user.name, mail.body.encoded
    assert_match @user.activation_token ,mail.body.encoded
    assert_match CGI.escape(@user.user), mail.body.encoded
  end

  test "password reset" do
    @user.reset_token = User.new_token
    mail= UserMailer.password_reset(@user)
    assert_equal "Password Reset from Dobble", mail.subject
    assert_equal ["ram48484@gmail.com"], mail.to
    assert_equal ["sairamreddy3048@gmail.com"], mail.from
    assert_match @user.name, mail.body.encoded
    assert_match @user.reset_token , mail.body.encoded
    assert_match CGI.escape(@user.user), mail.body.encoded
  end
  # test "password_reset" do
  #   mail = UserMailer.password_reset
  #   assert_equal "Password reset", mail.subject
  #   assert_equal ["to@example.org"], mail.to
  #   assert_equal ["from@example.com"], mail.from
  #   assert_match "Hi", mail.body.encoded
  # end

end

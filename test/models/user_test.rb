require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.new(name:"sairam",user: "sairamreddy@mallowtech.com",password: "Kings@123",password_confirmation: "Kings@123")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "Name should not present" do
    @user.name= "   "
    assert_not @user.valid?
  end

  test "Email should be present" do
    @user.user = nil
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "email must be short" do
    @user.user = "a" * 246 + "@gmail.com"
    assert_not @user.valid?
  end

  test "Test should accept valid email address" do
    valid_addresses = %w[sairam@gmail,com sairamreddymallow-tech..com sairamreddy@zoho...cocm sairamreddy@thethirdeyeindia+-fd34fsd'/;.comcom]
    valid_addresses.each do |address|
      @user.user = address
      assert_not @user.valid?, "#{address.inspect} should be valid"
    end
  end

  test "Email address should be unique" do
    duplicate_user = @user.dup
    duplicate_user.user = @user.user.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "should save as the downcase" do
    @me = User.new(name: "John", user: "sairaMREDDY@GM.com",password: "Kings@123", password_confirmation: "Kings@123")
    mail = @me.user.downcase!
    @me.save
    assert_equal mail,@me.reload.user
  end

  test "Should return false for a user with invalid email/token" do
    assert_not @user.authenticated?(:activation, '')
  end

  test "should destroy aa the microposts of the user" do
    @user.save
    @user.micro_posts.create(content:"hey there")
    assert_difference 'MicroPost.count',-1 do
      @user.destroy
    end
  end

  test "should follow and unfollow a user" do
    michael = users(:sairam)
    archer  = users(:murali)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    assert archer.followers.include?(michael)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end

  test "Feed should have right posts" do
    sai = users(:sairam)
    murali = users(:murali)
    user_1 = users(:user_1)
    sai.follow(murali)
    #for the followed user
    murali.micro_posts.each do |post|
      assert sai.feed.include?(post)
    end

    # Wherther my posts are included in the feed
    sai.micro_posts.each do |post|
      assert sai.feed.include?(post)
    end

    # Wherther unfollowed user posts are included in the feed
    user_1.micro_posts.each do |post|
      assert sai.feed.include?(post)
    end


  end

end

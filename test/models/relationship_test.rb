require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @relationship = Relationship.new(follower_id: users(:sairam).id,followed_id: users(:murali).id)
  end

  test "Should be Valid" do
    assert @relationship.valid?
  end

  test "Should require a follower_id" do
    @relationship.follower_id = nil
    assert_not @relationship.valid?
  end

  test "Should require a followed_id" do
    @relationship.followed_id = nil
    assert_not @relationship.valid?
  end

end

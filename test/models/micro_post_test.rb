require 'test_helper'

class MicroPostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:sairam)
    @micropost = @user.micro_posts.build(content:"hello dude")
  end

  test "Micropost should be valid" do
    assert @micropost.valid?
  end

  test "Micropost should be invalid" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end

  test "Content should present" do
    @micropost.content = ""
    assert_not @micropost.valid?
  end

  test "content should be less than 140 characters" do
    @micropost.content = "a" * 141
    assert_not @micropost.valid?
  end

  test "Most recent post first" do
    assert_equal micro_posts(:most_recent),MicroPost.first
  end
end
